=========================
Pointless Blog of Apricot
=========================

This is the repository for my blog postings of `Pointless Blog of Apricot`_. It
uses Apricot_ theme and tools.

.. _Pointless Blog of Apricot: https://pba.yjl.im
.. _Apricot: https://bitbucket.org/pelican-apricot/


.. contents:: **Contents**
   :local:


Copyright
=========

The contents in this repository have been placed in the public domain, or via
UNLICENSE_, if not applicable.

.. _UNLICENSE: UNLICENSE
