:title: My first static blog
:tags: Google, Pelican, Bitbucket, Aerobatic
:date: 2016-02-24T02:59:21Z

Roughly ten days ago, I was seriously thinking about moving on from my previous
blog on Blogger after Google announced the shutdown of `Picasa Web Albums`_. It
was the time for me to stop and walk on another path since I already wasn't
completely like Blogger platform [#blogger]_ and the Google has become.

.. _Picasa Web Albums: https://en.wikipedia.org/wiki/Picasa_Web_Albums

It took me days to search for a replacement, but I knew it wouldn't be exactly
the same, and probably I would have to spend a lot of time, and that's exactly
what I did for the last few days.

.. [#blogger]

   It injects too much of stuff in your blog and can't be a 100% valid HTML5.


Firstly, *what to write in?*
============================

I didn't want any server-side, just plain old and pure HTML to deliver my
content. Naturally, you probably would think about GitHub Pages, which is not a
bad choice, just I don't have the environment to run if I want to test,
especially at this very first stage.

Generating web pages on my end and sending them to hosting services for the
public is the better choice. I can choose any tools I like to use rather than
being forced to use the tool chosen by the hosting service.

I picked Pelican_ Static Site Generator, which is written in Python and I can
code in Python if I need to.

.. _Pelican: http://getpelican.com/


Secondly, *how to deliver?*
===========================

I went through several hosting services, but couldn't really pick one. I wanted
a simple static web hosting with custom domain support, but either they have
small storage size limitation or something that I don't like. My previous blog
has nearly 1,800 blog posts in about 8 years. I doubt I'd even exceed the
limitation, but I just don't want to take my chance, then I remember I'd read
somewhere about an addon on Bitbucket_, which is Aerobatic_, a static hosting
for Bitbucket.

.. _Bitbucket: https://bitbucket.org/
.. _Aerobatic: https://www.aerobatic.com/

After reading its pages, I learned they even provide SSL certificate which is
issued by Amazon AWS Certificate Manager, 1 domain with two sites for free
plan, the decision was made.

I only need one site for this blog, and maybe another for |www.yjl.im|_, which
currently is running on Google App Engine. Most part is static, only a feed
generator needs the engine. I am still thinking if I should sack that feed and
move the pages to Aerobatic as well, since I can't really find an equivalence
to GAE.

.. |www.yjl.im| replace:: ``www.yjl.im``
.. _www.yjl.im: http://www.yjl.im


Here I am
=========

This new blog is running, but there is still a lot of things need to be done,
tweaked, or fine-tuned. But for now, I need a break.
