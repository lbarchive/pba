:title: One brief month with Dailymotion
:tags: Dailymotion, YouTube, Google
:date: 2016-03-27T10:05:37Z


.. contents:: **Contents**


Why the move
============

On 2016-02-15, I decided to move to Dailymotion from YouTube, it all started when Google announced the shutdown of Picasa Web Albums. I wanted to move away anything Google, stayed clear of Google.

33 days later, I came back to YouTube. I wasn't very happy with YouTube as a content publisher, or I should say with Google. YouTube is too big and if you ain't a big channel, they won't hear you. It's not just YouTube, many Google's products or companies are the same, all you can get help from is the other users. They seem to hide themselves behind users, most of time you don't get official answers.

So, I bit the bullet to move just after I crossed the `1,000 video milestone`__.

__ https://www.youtube.com/watch?v=dAlnoh_Kp8I


Why moved back
==============

In short, *spam comments*.

During the time on Dailymotion before moving back, I received 42 comments and all of them are spams, and only one spam had triggered email notification. Because the lack of comments management and video manager, I'd have to go through all my videos in order to find out where the new comments were the new comments. I still could know if there is new comment using the Analytics, just not where it's left on.

That's the last straw to push me back to YouTube.

Aside: Copyrighted content detection
------------------------------------

After I came back, I decided to transfer all 28 videos back, but I noticed the last one (8 days after published) was rejected because it has a "breach of Terms of Service."

I submitted a request for an explanation, and this is what I got:

  [...] Your video contains audio elements protected by copyrights holders, and therefore has been rejected. You can find more information regarding our fingerprinting solutions here : http://www.dailymotion.com/legal/contentprotection

I can't believe what I was reading there, apparently their copyright protection system is seriously broken, because that video has *no audio track*, how on Earth it could match a copyrighted content using audio fingerprint is beyond me.

According to the link in the email response, their protection/detection system is built with two systems as listed below, and they claim to be "the first video hosting website to use two content filtering solutions simultaneously."

1. one by Audible Magic (since July, 2007)

     This filtering solution, based on the recognition of audio fingerprints, can automatically detect copyrighted soundtracks.

2. "Signature" by the INA (Institut National de l'audivisuel) (since January, 2008)

So, does that mean a video lack of audio tracks are copyrighted?

After I replied with the note that video has no audio track, I went to check videos and noticed that video is no longer being rejected. I don't know if that's before or after my reply, but 12 hours later, they replied, apologized for "for the inconvenience" and informed me that my video is available again.


Compared to YouTube
===================

When I started the move, some notes were made, but they don't matter to me anymore. However, they might still be informative.

Better or what I like
---------------------

* Characters counters for title and description.
* *Only 10 tags are allowed*. It prevents tagging abuse as it's very common on YouTube. As a viewer and who often searches, this really is better for viewers. 10 is more than enough, in fact, 5 could be a even better number in my opinion.
* *Password-protected privacy*. You can set a password to a video. Both YouTube and Dailymotion have public and private. The only difference is Unlisted and Password-protected between them.
* You can *replace a video* and keep the video ID unchanged, that you can't do on YouTube.
* *Publication date range* for visibility, and set up auto-deletion if needed.
* You choose the *next video* to play for viewers.
* Thumbnail slideshow on front page or other pages. I like this feature, you hover cursor on video thumbnail, and you can preview the video in slides. This helps avoid some thumbnail baits.

Worse or what I don't like
--------------------------

* No videos and comments managers.
* For Gaming category, game title must be in the predefined list.
* Can't create playlist in uploading page. You can only create in playlist section of your channel page.
* Some of auto-suggested tags are not fully translated into English.
* Custom thumbnail can only be uploaded in video editing page.
* No time code. You can't link to a video with specific starting point.
* Forced minimum of bitrate.
* Too many spams and seemingly no spam protection.

In the issues above, the biggest is the lack of management tool, which is very inconvenient. You can only manage through your channel pages, the videos section.

It's a huge issue for me. On YouTube, the videos manager is very helpful to glance the status of videos, especially the recent uploads. You know the views, comments, like/dislikes, plus monetization status. Also, you can quickly move video between playlists, even do a batch action.

Also a strange requirement for minimum of bitrate. As far as I can tell from what I have uploaded, below 257 kbps is rejected, above 308.84 kbps is accepted. The actual threshold is somewhere between them.

For most of videos, not screencast, they are likely would be above the bitrate. But since my videos are screencasts and fairly low fps, the bitrate often below 100 kbps. In order to get accepted, I beefed them up by encoding closer to lossless, which really is unnecessary since there is not much differences you can tell visually.


What I have learned
===================

I don't think I would move anytime soon even I do want to stay away, 33 days and 28 videos, it's not much efforts, but it's frustrating because I thought Dailymotion is a good alternative, but the fact shows that it's not good enough. 

Monopoly is bad, YouTube dominates the video hosting and, unfortunately, even I had tried to leave, I still have to come back. I am worrying that Dailymotion would be gone someday, spams are a huge issue and Dailymotion have been around so many years, still that amount of spam is unbelievable. For me, it's fine as long as you can moderate it, but that's exactly what Dailymotion is lacking. Not only I see no email notifications, but also you don't know where the spam comments are left on.

How could you work with a system like that?
