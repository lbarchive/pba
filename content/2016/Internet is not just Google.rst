:title: Internet is not just the Google
:tags: Google
:date: 2016-02-27T00:12:49Z

On 2016-02-16, Google announced `the end of Picasa Web Albums`__ and I have
enough of Google.

__ http://googlephotos.blogspot.com/2016/02/moving-on-from-picasa.html

Not sure how many Google had killed and how many of them were bought by Google
and then suffocated, but I know Picasa / Web Albums wouldn't be the last.

This isn't a post accusing Google of being bad, just a post saying that Google
is no different than other companies. In otther words, it is as bad as others.

The Google we knew ten years ago have already gone, and I don't believe it
would just stop being the Google, it would be far beyond that if not already.
Not just buying companies. I finally begin to understand why years ago why some
people telling Google off, which I didn't see at the time.

I don't know if you have noticed that over the years, `Google logo`_ has been
heading to the direction of simpler design, sadly, it's not the same thing for
the company which gets more complicated every year. Isn't that ironic? On the
face, it looks simple, but deep inside, nothing is what you see on the surface.

.. _Google logo: https://en.wikipedia.org/wiki/Google_logo

I am trying to stay clear of Google as best as I can, but some are just not
easy to find an alternative, for instance, Gmail, when you have been using it
for more than a decade, even there is plenty of choices, old habits die hard.

So far, I have left Blogger and YouTube, and stop actively using Google App
Engine, Google Search, and Google Webmaster Tools. I realize that I couldn't
completely cut *everything Google* off, it just won't be viable. At least, I
won't be signing up for new services they come up, or creating new contents on
Google. For whatever is already there, I will continue to use until I can find
a replacement.

Even I would have succeeded to move on, there is no escape, because I'd still
be using other companies' services and in the end, they are companies, just
like the old Google, for now.
