#########
# Basic #
#########

AUTHOR = 'Yu-Jie Lin'
USE_FOLDER_AS_CATEGORY = False
DEFAULT_CATEGORY = 'assorted'
STATIC_PATHS = [
  'static/UNLICENSE',
  'static/robots.txt',
]
EXTRA_PATH_METADATA = {
  'static/UNLICENSE': {'path': 'UNLICENSE.txt'},
  'static/robots.txt': {'path': 'robots.txt'},
}
IGNORE_FILES = ['.*.swp']
PATH = 'content'
SITENAME = 'Pointless Blog of Apricot'
SITEURL = 'https://pba.yjl.im'
TIMEZONE = 'UTC'

DIRECT_TEMPLATES = ['index', 'categories', 'tags', 'archives']

CACHE_CONTENT = True
LOAD_CONTENT_CACHE = True

#######
# URL #
#######

ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{slug}.html'
PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'
AUTHOR_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
YEAR_ARCHIVE_SAVE_AS = '{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = '{date:%Y}/{date:%m}/index.html'

##############
# Pagination #
##############

DEFAULT_PAGINATION = 10

################
# Translations #
################

DEFAULT_LANG = 'en'

##########
# Themes #
##########

THEME = './apricot'
THEME_STATIC_DIR = ''

###########
# Plugins #
###########

PLUGIN_PATHS = ['plugins']
PLUGINS = ['rst-kbd']

#################
# Apricot theme #
#################

CSS_INLINE = True

COPYRIGHT = (
  '<small>Unless otherwise stated, '
  'all contents have been placed in the public domain, '
  "or via <a rel='license' href='/UNLICENSE.txt'><code>UNLICENSE</code></a>, "
  'if not applicable.</small>'
)

